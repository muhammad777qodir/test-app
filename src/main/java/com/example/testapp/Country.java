package com.example.testapp;

import lombok.Data;

@Data
public class Country {

    private Card card;

    private Sapp2Card sapp2Card;

    private Sapp3Card sapp3Card;

    private Sapp6Card sapp6Card;

    private Sapp5Card sapp5Card;

    private Sapp7Card sapp7Card;


    private Payment payment;

    public Country(
            Card card,
            Sapp2Card sapp2Card,
            Sapp3Card sapp3Card,
            Sapp6Card sapp6Card,
            Sapp5Card sapp5Card,
            Payment payment,
            Sapp7Card sapp7Card
    ){
        this.card = card;
        this.sapp2Card = sapp2Card;
        this.sapp3Card = sapp3Card;
        this.sapp6Card = sapp6Card;
        this.sapp5Card = sapp5Card;
        this.sapp7Card = sapp7Card;
        this.payment = payment;
    }


    public String cardMethod(){
        return "card";
    }

    public String sapp2Method(){
        return "sapp2";
    }

    public String sapp3Method(){
        return "sapp3card";
    }

    public String sapp6Method(){
        return "sapp6card";
    }

    public String sapp5Method(){
        return "sapp5card";
    }

    public String sapp7Method(){
        return "sapp7card";
    }


    public String paymentMethod(){
        return "payment";
    }

}
